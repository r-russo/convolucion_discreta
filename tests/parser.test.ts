import { expect, test } from "vitest";
import { parser_evaluate } from "../src/parser";

test("operadores unarios", () => {
  expect(parser_evaluate("+2+2", 1)?.[0]).toBe(4);
  expect(parser_evaluate("-2+2", 1)?.[0]).toBe(0);
  expect(parser_evaluate("-2-(-2)", 1)?.[0]).toBe(0);
});

test("operaciones compuestas", () => {
  expect(parser_evaluate("(1+2)*2", 1)?.[0]).toBe(6);
  expect(parser_evaluate("(10+20)*2", 1)?.[0]).toBe(60);
  expect(parser_evaluate("(1+2)*2^2", 1)?.[0]).toBe(12);
  expect(parser_evaluate("(1+(3-2)^2*5/4)^0.5", 1)?.[0]).toBe(1.5);
});

test("funciones", () => {
  expect(parser_evaluate("2*sin(3.14159/2)", 1)?.[0]).toBe(2 * Math.sin(3.14159 / 2));
});

test("variable", () => {
  expect(parser_evaluate("2*n+1", 10)).toEqual(new Array(10).fill(0).map((_, i) => 2 * i + 1));
  expect(parser_evaluate("2*u(n) - u(n-2)", 5)).toEqual([2, 2, 1, 1, 1]);
  expect(parser_evaluate("d(n) + d(n-1)", 5)).toEqual([1, 1, 0, 0, 0]);
  expect(parser_evaluate("r(n) - r(n-2)", 5)).toEqual([0, 1, 2, 2, 2]);
});
