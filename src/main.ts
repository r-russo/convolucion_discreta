import { parser_evaluate } from "./parser";
import { stem } from "./plot";
import { Signal, SignalElements } from "./types";
import { conv, convk } from "./utils";

const xn: Signal = {
  n: [],
  data: [],
};

const xn_elements: SignalElements = {
  input: document.querySelector<HTMLInputElement>("#input_xn")!,
  spin_n: document.querySelector<HTMLInputElement>("#spin_n_xn")!,
  error_log: document.querySelector<HTMLSpanElement>("#error_log_xn"),
};

const hn: Signal = {
  n: [],
  data: [],
};

const hn_elements: SignalElements = {
  input: document.querySelector<HTMLInputElement>("#input_hn")!,
  spin_n: document.querySelector<HTMLInputElement>("#spin_n_hn")!,
  error_log: document.querySelector<HTMLSpanElement>("#error_log_hn"),
};

const slider_k = document.querySelector<HTMLInputElement>("#slider_k")!;
const label_slider_k = document.querySelector<HTMLInputElement>("#label_slider_k")!;
const canvas = document.querySelector("canvas");

const evaluate_expression = (s: Signal, el: SignalElements) => {
  if (el.spin_n.valueAsNumber > parseInt(el.spin_n.max)) {
    el.spin_n.value = el.spin_n.max;
  }

  if (el.spin_n.valueAsNumber < parseInt(el.spin_n.min)) {
    el.spin_n.value = el.spin_n.min;
  }

  let data = parser_evaluate(el.input.value, el.spin_n.valueAsNumber, (error: string) => {
    if (el.error_log != null) {
      el.error_log.innerText = error;
    }
    if (error.length == 0) {
      el.input.classList.remove("error");
    } else {
      el.input.classList.add("error");
    }
  });

  if (data == null) {
    return;
  }

  s.n = new Array(data.length).fill(0).map((_, i) => i);
  s.data = data;
};

const update = () => {
  const L = xn.n.length;
  const M = hn.n.length;
  const N = L + M - 1;
  slider_k.min = "0";
  slider_k.max = (N - 1).toString();
  label_slider_k.innerText = `k = ${slider_k.valueAsNumber}`;

  draw();
};

const draw = () => {
  if (canvas == null || xn.data.length == 0 || hn.data.length == 0) {
    return;
  }

  const ctx = canvas.getContext("2d");
  if (ctx == null) {
    return;
  }

  const zero_pad = (x: number[], left: number, right: number): number[] => {
    const r = new Array(left).fill(0);
    r.push(...x);
    r.push(...new Array(right).fill(0));
    return r.slice();
  };

  const plot_w = canvas.width;
  const plot_h = canvas.height / 4;

  ctx.reset();
  ctx.clearRect(0, 0, plot_w, plot_h);

  const L = xn.data.length;
  const M = hn.data.length;
  const N = L + M - 1;

  const k = slider_k.valueAsNumber;

  const x = zero_pad(xn.data, M - 1, N + 1 - xn.data.length);
  const h = zero_pad(hn.data.slice().reverse(), k, N + 1 + M - 1 - hn.data.length - k);
  const n = new Array(x.length).fill(0).map((_, i) => i - M + 1);
  const prod = zero_pad(convk(xn.data, hn.data, k), M - 1, 1);
  const y = zero_pad(conv(xn.data, hn.data), M - 1, 1);

  const font_family = "Fira Sans";

  stem(ctx, { x: 0, y: 0, w: plot_w, h: plot_h }, n, x, {
    xlabel: "n",
    title: "x(n)",
    color: "#a759ff",
    font_family: font_family,
  });
  stem(ctx, { x: 0, y: 1 * plot_h, w: plot_w, h: plot_h }, n, h, {
    xlabel: "k",
    title: "h(n-k)",
    color: "#3584E3",
    font_family: font_family,
  });
  stem(ctx, { x: 0, y: 2 * plot_h, w: plot_w, h: plot_h }, n, prod, {
    xlabel: "k",
    title: "x(k) * h(n - k)",
    color: "#FA4700",
    font_family: font_family,
  });
  stem(ctx, { x: 0, y: 3 * plot_h, w: plot_w, h: plot_h }, n, y, {
    xlabel: "n",
    title: "y(n)",
    color: "#aaaaaa",
    font_family: font_family,
  });
  stem(ctx, { x: 0, y: 3 * plot_h, w: plot_w, h: plot_h }, [n[k + M - 1]], [y[k + M - 1]], {
    color: "#16A300",
    xaxis: false,
    yaxis: false,
    xticks: false,
    yticks: false,
    xrange: [Math.min(...n), Math.max(...n)],
    yrange: [Math.min(...y), Math.max(...y)],
  });
};

const connect_inputs_and_update = (s: Signal, el: SignalElements) => {
  const eval_and_update = (s: Signal, el: SignalElements) => {
    evaluate_expression(s, el);
    update();
  };
  el.input.oninput = (_ev: Event) => eval_and_update(s, el);
  el.spin_n.oninput = (_ev: Event) => eval_and_update(s, el);

  eval_and_update(s, el);
};

xn_elements.input.value = "n";
xn_elements.spin_n.value = "5";
hn_elements.input.value = "u(n)";
hn_elements.spin_n.value = "5";
slider_k.value = "0";

connect_inputs_and_update(xn, xn_elements);
connect_inputs_and_update(hn, hn_elements);
slider_k.oninput = update;

window.onload = window.onresize = function () {
  canvas!.style.width = "100%";
  canvas!.style.height = "100%";
  canvas!.width = canvas!.offsetWidth;
  canvas!.height = canvas!.offsetHeight;
  update();
};
