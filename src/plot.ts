import { Rect } from "./types";

export const map_to_range = (v: number, from: [number, number], to: [number, number]): number => {
  return ((v - from[0]) / (from[1] - from[0])) * (to[1] - to[0]) + to[0];
};

export type PlotProps = {
  xrange: [number, number];
  yrange: [number, number];
  color: string;
  xaxis: boolean;
  yaxis: boolean;
  xticks: boolean;
  yticks: boolean;
  n_xticks: number;
  n_yticks: number;
  xlabel: string;
  ylabel: string;
  title: string;
  font_family: string;
  font_size_px: number;
};

const DEFAULT_PLOT_PROPS: PlotProps = {
  xrange: [0, 1],
  yrange: [0, 1],
  color: "blue",
  xaxis: true,
  yaxis: true,
  xticks: true,
  yticks: true,
  n_xticks: 10,
  n_yticks: 5,
  ylabel: "",
  xlabel: "",
  title: "",
  font_family: "sans",
  font_size_px: 16,
};

const mx = 40;
const my = 40;

const _x_to_plot = (v: number, r: Rect, p: PlotProps) => {
  // HACK:
  const xrange = _calculate_range(p.xrange[0], p.xrange[1], p.n_xticks);
  return map_to_range(v, xrange, [r.x, r.x + r.w]);
};

const _y_to_plot = (v: number, r: Rect, p: PlotProps) => {
  const yrange = _calculate_range(p.yrange[0], p.yrange[1], p.n_yticks);
  return map_to_range(v, yrange, [r.y + r.h, r.y]);
};

const _nice_number = (value: number, round: boolean): number => {
  const exponent = Math.floor(Math.log10(value));
  const fraction = value / Math.pow(10, exponent);
  let nice_frac = 0;

  if (round) {
    if (fraction < 1.5) {
      nice_frac = 1;
    } else if (fraction < 3) {
      nice_frac = 2;
    } else if (fraction < 7) {
      nice_frac = 5;
    } else {
      nice_frac = 10;
    }
  } else {
    if (fraction < 1) {
      nice_frac = 1;
    } else if (fraction < 2) {
      nice_frac = 2;
    } else if (fraction < 5) {
      nice_frac = 5;
    } else {
      nice_frac = 10;
    }
  }
  return nice_frac * Math.pow(10, exponent);
};

const _calculate_range = (min: number, max: number, nticks: number): [number, number] => {
  if (min == max) {
    return [min - 0.1, max + 0.1];
  }
  const range = _nice_number(max - min, false);
  const spacing = _nice_number(range / (nticks - 1), true);
  const graph_min = Math.floor(min / spacing) * spacing;
  const graph_max = Math.ceil(max / spacing) * spacing;

  return [graph_min, graph_max];
};

const _calculate_ticks = (min: number, max: number, nticks: number): number[] => {
  if (min == max) {
    return [min - 0.1, max + 0.1];
  }
  const range = _nice_number(max - min, false);
  const spacing = _nice_number(range / (nticks - 1), true);
  const graph_min = Math.floor(min / spacing) * spacing;
  const graph_max = Math.ceil(max / spacing) * spacing;
  const nfrac = Math.max(-Math.floor(Math.log10(spacing)), 0);
  const pow_digits = Math.pow(10, nfrac);
  const ticks = [];

  for (let x = graph_min; x < graph_max + 0.5 * spacing; x += spacing) {
    ticks.push(Math.round(x * pow_digits) / pow_digits);
  }

  return ticks;
};

const _validate_data = (x: number[], y: number[]) => {
  if (x.length != y.length) {
    console.error("[plot] x and y must match in size");
    return false;
  }
  return true;
};

const _validate_plot_props = (x: number[], y: number[], props: PlotProps): PlotProps => {
  const p: PlotProps = {
    xrange: props.xrange ?? [Math.min(...x), Math.max(...x)],
    yrange: props.yrange ?? [Math.min(...y), Math.max(...y)],
    xaxis: props.xaxis ?? DEFAULT_PLOT_PROPS.xaxis,
    yaxis: props.yaxis ?? DEFAULT_PLOT_PROPS.yaxis,
    xticks: props.xticks ?? DEFAULT_PLOT_PROPS.xticks,
    yticks: props.yticks ?? DEFAULT_PLOT_PROPS.yticks,
    color: props.color ?? DEFAULT_PLOT_PROPS.color,
    n_xticks: props.n_xticks ?? DEFAULT_PLOT_PROPS.n_xticks,
    n_yticks: props.n_yticks ?? DEFAULT_PLOT_PROPS.n_yticks,
    xlabel: props.xlabel ?? DEFAULT_PLOT_PROPS.xlabel,
    ylabel: props.ylabel ?? DEFAULT_PLOT_PROPS.ylabel,
    title: props.title ?? DEFAULT_PLOT_PROPS.title,
    font_family: props.font_family ?? DEFAULT_PLOT_PROPS.font_family,
    font_size_px: props.font_size_px ?? DEFAULT_PLOT_PROPS.font_size_px,
  };

  return p;
};

const _draw_axes = (ctx: CanvasRenderingContext2D, r: Rect, props: PlotProps) => {
  ctx.save();
  ctx.strokeStyle = "black";
  const [xmin, xmax] = _calculate_range(props.xrange[0], props.xrange[1], props.n_xticks);
  const [ymin, ymax] = _calculate_range(props.yrange[0], props.yrange[1], props.n_yticks);
  const left = _x_to_plot(xmin, r, props);
  const right = _x_to_plot(xmax, r, props);
  const top = _y_to_plot(ymax, r, props);
  const bottom = _y_to_plot(ymin, r, props);
  const origin = [_x_to_plot(0, r, props), _y_to_plot(0, r, props)];
  if (props.xaxis) {
    ctx.beginPath();
    ctx.moveTo(left, origin[1]);
    ctx.lineTo(right, origin[1]);
    ctx.stroke();
  }

  if (props.yaxis) {
    ctx.beginPath();
    ctx.moveTo(origin[0], top);
    ctx.lineTo(origin[0], bottom);
    ctx.stroke();
  }
  ctx.restore();
};

const _draw_ticks = (ctx: CanvasRenderingContext2D, r: Rect, props: PlotProps) => {
  ctx.save();
  ctx.strokeStyle = "black";
  ctx.textAlign = "center";
  ctx.font = `${props.font_size_px}px ${props.font_family}`;
  const origin = [_x_to_plot(0, r, props), _y_to_plot(0, r, props)];
  if (props.xticks) {
    const xticks = _calculate_ticks(props.xrange[0], props.xrange[1], props.n_xticks);
    xticks.forEach((x) => {
      if (x == 0) {
        return;
      }
      ctx.beginPath();
      ctx.moveTo(_x_to_plot(x, r, props), origin[1] - 3);
      ctx.lineTo(_x_to_plot(x, r, props), origin[1] + 3);
      ctx.stroke();

      ctx.fillText(x.toString(), _x_to_plot(x, r, props), origin[1] + props.font_size_px * 1.5);
    });
  }

  if (props.yticks) {
    const yticks = _calculate_ticks(props.yrange[0], props.yrange[1], props.n_yticks);
    yticks.forEach((y) => {
      if (y == 0) {
        return;
      }
      ctx.beginPath();
      ctx.moveTo(origin[0] - 3, _y_to_plot(y, r, props));
      ctx.lineTo(origin[0] + 3, _y_to_plot(y, r, props));
      ctx.stroke();
      ctx.textAlign = "center";
      ctx.fillText(y.toString(), origin[0] - 25, _y_to_plot(y, r, props) + props.font_size_px / 2);
    });
  }
  ctx.restore();
};

const _draw_labels = (ctx: CanvasRenderingContext2D, r: Rect, props: PlotProps) => {
  ctx.save();
  const origin = [_x_to_plot(0, r, props), _y_to_plot(0, r, props)];
  const [_xmin, xmax] = _calculate_range(props.xrange[0], props.xrange[1], props.n_xticks);
  const [_ymin, ymax] = _calculate_range(props.yrange[0], props.yrange[1], props.n_yticks);
  const right = _x_to_plot(xmax, r, props);
  const top = _y_to_plot(ymax, r, props);

  ctx.strokeStyle = "black";
  ctx.textAlign = "right";
  ctx.fillStyle = "black";
  ctx.font = `${props.font_size_px}px ${props.font_family}`;
  if (props.xlabel.length > 0) {
    ctx.fillText(props.xlabel, right + 32, origin[1] + 24);
  }
  ctx.textAlign = "left";
  ctx.textBaseline = "middle";
  if (props.ylabel.length > 0) {
    ctx.fillText(props.ylabel, origin[0] + props.font_size_px, top);
  }

  ctx.restore();
};

const _draw_title = (ctx: CanvasRenderingContext2D, r: Rect, props: PlotProps) => {
  ctx.save();
  ctx.font = `bold ${props.font_size_px}px ${props.font_family}`;
  ctx.textAlign = "center";
  ctx.textBaseline = "top";
  if (props.title.length > 0) {
    ctx.fillText(props.title, r.x + r.w / 2, r.y + 8);
  }
  ctx.restore();
};

const _prepare_plot = (ctx: CanvasRenderingContext2D, r: Rect, plot_rect: Rect, props: PlotProps) => {
  _draw_axes(ctx, plot_rect, props);
  _draw_ticks(ctx, plot_rect, props);
  _draw_labels(ctx, plot_rect, props);
  _draw_title(ctx, r, props);
};

export const plot = (ctx: CanvasRenderingContext2D, r: Rect, x: number[], y: number[], plot_props?: any) => {
  if (!_validate_data(x, y)) {
    return;
  }

  const props = _validate_plot_props(x, y, plot_props ?? DEFAULT_PLOT_PROPS);
  const plot_rect: Rect = { x: r.x + mx, y: r.y + my, w: r.w - 2 * mx, h: r.h - 2 * my };

  _prepare_plot(ctx, r, plot_rect, props);

  ctx.save();
  ctx.fillStyle = props.color;
  ctx.strokeStyle = props.color;
  ctx.beginPath();
  for (let i = 0; i < x.length; i++) {
    ctx.lineTo(_x_to_plot(x[i], plot_rect, props), _y_to_plot(y[i], plot_rect, props));
  }
  ctx.stroke();
  ctx.restore();
};

export const stem = (ctx: CanvasRenderingContext2D, r: Rect, x: number[], y: number[], plot_props?: any) => {
  if (!_validate_data(x, y)) {
    return;
  }

  const props = _validate_plot_props(x, y, plot_props ?? DEFAULT_PLOT_PROPS);
  const plot_rect: Rect = { x: r.x + mx, y: r.y + my, w: r.w - 2 * mx, h: r.h - 2 * my };

  _prepare_plot(ctx, r, plot_rect, props);

  ctx.save();

  const origin = [_x_to_plot(0, plot_rect, props), _y_to_plot(0, plot_rect, props)];
  ctx.fillStyle = props.color;
  ctx.strokeStyle = props.color;
  for (let i = 0; i < x.length; i++) {
    ctx.beginPath();
    ctx.arc(_x_to_plot(x[i], plot_rect, props), _y_to_plot(y[i], plot_rect, props), 5, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(_x_to_plot(x[i], plot_rect, props), origin[1]);
    ctx.lineTo(_x_to_plot(x[i], plot_rect, props), _y_to_plot(y[i], plot_rect, props));
    ctx.stroke();
  }
  ctx.restore();
};
