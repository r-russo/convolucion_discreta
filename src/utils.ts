export const convk = (x: number[], h: number[], k: number): number[] => {
  const N = x.length + h.length - 1;

  let y = new Array(N).fill(0);

  for (let i = 0; i < N; i++) {
    y[i] += (x[i] ?? 0) * (h[k - i] ?? 0);
  }

  return y;
};

export const conv = (x: number[], h: number[]): number[] => {
  const N = x.length + h.length - 1;

  let y = new Array(N).fill(0);

  for (let n = 0; n < N; n++) {
    for (let k = 0; k < N; k++) {
      y[n] += (x[k] ?? 0) * (h[n - k] ?? 0);
    }
  }

  return y;
};
