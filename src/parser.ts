const isNumeric = (n: string): boolean => {
  return !isNaN(Number(n)) && !isNaN(parseFloat(n));
};

type Operators = {
  [key: string]: {
    precedence: number;
    fun: (a: number[], b: number[]) => number[];
    assoc: "left" | "right";
    is_unary: boolean;
  };
};

type Functions = {
  [key: string]: {
    num_args: number;
    fun: (args: number[][]) => number[];
  };
};

const operators: Operators = {
  "+": {
    precedence: 2,
    fun: (a, b) => a.map((v, i) => v + b[i]),
    assoc: "left",
    is_unary: false,
  },
  "-": {
    precedence: 2,
    fun: (a, b) => a.map((v, i) => v - b[i]),
    assoc: "left",
    is_unary: false,
  },
  "*": {
    precedence: 3,
    fun: (a, b) => a.map((v, i) => v * b[i]),
    assoc: "left",
    is_unary: false,
  },
  "/": {
    precedence: 3,
    fun: (a, b) => a.map((v, i) => v / b[i]),
    assoc: "left",
    is_unary: false,
  },
  "^": {
    precedence: 4,
    fun: (a, b) => a.map((v, i) => v ** b[i]),
    assoc: "right",
    is_unary: false,
  },
  p: {
    precedence: 10,
    fun: (a, _b) => a,
    assoc: "right",
    is_unary: true,
  },
  m: {
    precedence: 10,
    fun: (a, _b) => a.map((v) => -v),
    assoc: "right",
    is_unary: true,
  },
};

const functions: Functions = {
  sin: {
    num_args: 1,
    fun: (args) => args[0].map((v) => Math.sin(v)),
  },
  rand: {
    num_args: 1,
    fun: (args) => args[0].map((_) => Math.random()),
  },
  exp: {
    num_args: 1,
    fun: (args) => args[0].map((v) => Math.exp(v)),
  },
  u: {
    num_args: 1,
    fun: (args) => args[0].map((v) => (v >= 0 ? 1 : 0)),
  },
  d: {
    num_args: 1,
    fun: (args) => args[0].map((v) => (v == 0 ? 1 : 0)),
  },
  r: {
    num_args: 1,
    fun: (args) => args[0].map((v) => (v >= 0 ? v : 0)),
  },
};

export const parser_evaluate = (expr: string, n: number, error_handler?: (error: string) => void): number[] | null => {
  if (expr.length == 0) {
    error_handler?.("");
    return new Array(n).fill(0);
  }

  let operator_stack = [];
  let output_queue = [];

  const re = new RegExp("^([\\d.]+|^[A-Za-z]\\w+)");

  let cnt_args = 1;
  let i = 0;
  let last_char = "";
  let token = "";
  while (i < expr.length) {
    last_char = token.length == 0 ? "" : token[token.length - 1];
    token = "";

    let re_result = re.exec(expr.slice(i));

    if (re_result != null) {
      token += re_result[0];
    } else {
      token = expr[i];
    }

    i += token.length;

    if (isNumeric(token)) {
      output_queue.push(token);
      continue;
    }

    if (token == "n") {
      // variable
      output_queue.push(token);
      continue;
    }

    if (token in functions) {
      operator_stack.push(token);
      continue;
    }

    if (token in operators) {
      if (last_char in operators || last_char == "" || last_char == "(") {
        if (token == "+") {
          token = "p";
        } else if (token == "-") {
          token = "m";
        }
      }
      const o1 = token;
      let o2 = operator_stack[operator_stack.length - 1];
      while (
        operator_stack.length > 0 &&
        o2 !== "(" &&
        operators[o2]?.precedence >= operators[o1]?.precedence &&
        operators[o1]?.assoc === "left"
      ) {
        operator_stack.pop();
        output_queue.push(o2);
        o2 = operator_stack[operator_stack.length - 1];
      }
      operator_stack.push(o1);
    } else if (token === ",") {
      let o2 = operator_stack[operator_stack.length - 1];
      cnt_args++;
      while (o2 !== "(") {
        operator_stack.pop();
        output_queue.push(o2);
        o2 = operator_stack[operator_stack.length - 1];
      }
    } else if (token === "(") {
      operator_stack.push(token);
    } else if (token === ")") {
      let o2 = operator_stack[operator_stack.length - 1];
      while (o2 !== "(") {
        if (operator_stack.length === 0) {
          error_handler?.("Mismatched parenthesis");
          return null;
        }
        operator_stack.pop();
        output_queue.push(o2);
        o2 = operator_stack[operator_stack.length - 1];
      }

      if (o2 != "(") {
        error_handler?.("Mismatched parenthesis");
        return null;
      }

      operator_stack.pop();
      const top = operator_stack[operator_stack.length - 1];
      if (top in functions) {
        if (cnt_args !== functions[top].num_args) {
          error_handler?.(
            `wrong number of arguments for function ${top}. expected ${functions[top].num_args} got ${cnt_args}`,
          );
          return null;
        }
        cnt_args = 1;
        operator_stack.pop();
        output_queue.push(top);
      }
    } else if (token === " ") {
      continue;
    } else {
      error_handler?.(`invalid expression near token "${token}" at ${i}`);
      return null;
    }
  }

  while (operator_stack.length > 0) {
    const top = operator_stack[operator_stack.length - 1];
    if (top === "(") {
      error_handler?.("Mismatched parenthesis");
      return null;
    }
    output_queue.push(top);
    operator_stack.pop();
  }

  let stack: number[][] = [];
  for (let token of output_queue) {
    if (token in operators) {
      let a: number[], b: number[];
      if (operators[token].is_unary) {
        b = new Array(n).fill(0);
        a = stack.pop() ?? new Array(n).fill(NaN);
      } else {
        b = stack.pop() ?? new Array(n).fill(NaN);
        a = stack.pop() ?? new Array(n).fill(NaN);
      }
      if (a.filter((v) => isNaN(v)).length > 0 || b.filter((v) => isNaN(v)).length > 0) {
        error_handler?.(`invalid operand near ${i}`);
        return null;
      }
      stack.push(operators[token].fun(a, b));
    } else if (token in functions) {
      let args: number[][] = [];
      for (let i = 0; i < functions[token].num_args; i++) {
        args.push(stack.pop() ?? new Array(n).fill(NaN));
      }
      stack.push(functions[token].fun(args));
    } else if (token == "n") {
      stack.push(new Array(n).fill(0).map((_, i) => i));
    } else {
      stack.push(new Array(n).fill(parseFloat(token)));
    }
  }

  error_handler?.("");
  return stack[0];
};
