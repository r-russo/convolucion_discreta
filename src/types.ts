export type Rect = {
  x: number;
  y: number;
  w: number;
  h: number;
};

export type Signal = {
  n: number[];
  data: number[];
};

export type SignalElements = {
  input: HTMLInputElement;
  spin_n: HTMLInputElement;
  error_log: HTMLSpanElement | null;
};
